package com.asciiart.image.loader

import com.asciiart.image.Image

trait ImageLoader {

    def load(): Image
}
