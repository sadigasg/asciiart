package com.asciiart.image.loader

import com.asciiart.image.Image
import com.asciiart.image.pixel.format.GrayscalePixel

import java.io.File
import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer

class FileLoader(filepath: String) extends ImageLoader {

    override
    def load(): Image = {

        val file = new File(filepath)

        if (!file.exists()) {
            //println("File ("+filepath+") doesn't exist")
            return null
        }
        val image = ImageIO.read(file)
        if (image == null) {
            println("Unsupported file format")
            return null
        }
        val dimX = image.getWidth
        val dimY = image.getHeight
        var pixel2DArray = ArrayBuffer[Array[GrayscalePixel]]()
        for (y <- 0 until dimY) {
            var pixelLine = ArrayBuffer[GrayscalePixel]()
            for (x <- 0 until dimX) {
                pixelLine.addOne(new GrayscalePixel(image.getRGB(x, y)))
            }
            pixel2DArray.addOne(pixelLine.toArray)
        }
        new Image(dimX, dimY, pixel2DArray.toArray)
    }
}
