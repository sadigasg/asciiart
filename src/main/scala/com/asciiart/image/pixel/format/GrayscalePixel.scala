package com.asciiart.image.pixel.format

class GrayscalePixel extends Pixel {

    private var _luminance: Int = 0

    def this(rgb: Int) = {
        this()
        val red = (rgb & 0xFF0000) >> 16
        val green = (rgb & 0xFF00) >> 8
        val blue = rgb & 0xFF
        _luminance = ((0.3 * red) + (0.59 * green) + (0.11 * blue)).toInt
        this.clamp()
    }

    def luma: Int = {
        _luminance
    }

    def luma_=(newLum: Int) = {
        _luminance = newLum
        this.clamp()
    }

    def clamp(): Unit = {
        if (this._luminance > 255) {
            this._luminance = 255
        }
        if (this._luminance < 0) {
            this._luminance = 0
        }
    }
}
