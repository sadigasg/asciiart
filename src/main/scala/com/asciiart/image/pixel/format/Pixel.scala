package com.asciiart.image.pixel.format

trait Pixel {

  def luma: Int
  def luma_=(newLum: Int)
}
