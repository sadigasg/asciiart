package com.asciiart.image.pixel.filter

import com.asciiart.image.pixel.format.{GrayscalePixel, Pixel}

class GrayscaleInverse() extends GrayscalePixelFilter {

    override
    def apply(pixel: GrayscalePixel): Unit = {

        pixel.luma = 255 - pixel.luma
    }
}
