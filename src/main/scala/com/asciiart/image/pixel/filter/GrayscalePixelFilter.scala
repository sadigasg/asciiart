package com.asciiart.image.pixel.filter

import com.asciiart.image.pixel.format.{GrayscalePixel, Pixel}

trait GrayscalePixelFilter {

    def apply(pixel: GrayscalePixel)
}
