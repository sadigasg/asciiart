package com.asciiart.image

import com.asciiart.image.pixel.filter.GrayscalePixelFilter
import com.asciiart.image.pixel.format.GrayscalePixel
import com.asciiart.image.transform._

import scala.collection.mutable.ArrayBuffer

class Image extends Scalable with Rotatable with Flipable {

    var dimX: Int = 0
    var dimY: Int = 0
    var pixelArray: Array[Array[GrayscalePixel]] = new Array[Array[GrayscalePixel]](0)

    def this(
        iDimX: Int,
        iDimY: Int,
        iPixelArray: Array[Array[GrayscalePixel]]
    ) = {
            this()
            if (iDimX < 1 || iDimY < 1) {
            throw new Exception("Unsupported resolution.")
        }
        if (iPixelArray.length != iDimY) {
            throw new Exception("Pixel array has wrong Y dimension.")
        }

        for (line <- iPixelArray) {
            if (line.length != iDimX) {
                throw new Exception("Pixel array has wrong X dimension.")
            }
        }

        this.dimX = iDimX
        this.dimY = iDimY
        this.pixelArray = iPixelArray
    }

    def applyFilter(pixelFilter: GrayscalePixelFilter): Unit = {
        for (y <- 0 until dimY) {
            for (x <- 0 until dimX) {
                pixelFilter.apply(pixelArray(y)(x))
            }
        }
    }

    def rotate(factor: RotationFactor): Unit = {
        factor match {
            case Deg_90 => {
                this.rotate90()
            }
            case Deg_180 => {
                this.rotate90()
                this.rotate90()
            }
            case Deg_270 => {
                this.rotate90()
                this.rotate90()
                this.rotate90()
            }
        }
    }

    def getPixel(y: Int)(x: Int): GrayscalePixel = {
        pixelArray(y)(x)
    }

    def scale(factor: ScalingFactor): Unit = {
        factor match {
            case Half => {
                this.scaleHalf()
            }
            case Twice => {
                this.scaleTwice()
            }
            case _ => {
                throw new Exception("Unknown scaling factor")
            }
        }
    }

    def flip(flip: AxisFlip): Unit = {
        flip match {
            case XFlip => this.flipX()
            case YFlip => this.flipY()
        }
    }

    private def flipX(): Unit = {
        var pixel2DArray = ArrayBuffer[Array[GrayscalePixel]]()
        for (y <- 0 until dimY) {
            var pixelLine = ArrayBuffer[GrayscalePixel]()
            for (x <- 0 until dimX) {
                pixelLine.addOne(pixelArray(dimY - 1 - y)(x))
            }
            pixel2DArray.addOne(pixelLine.toArray)
        }
        pixelArray = pixel2DArray.toArray
    }

    private def flipY(): Unit = {
        var pixel2DArray = ArrayBuffer[Array[GrayscalePixel]]()
        for (y <- 0 until dimY) {
            var pixelLine = ArrayBuffer[GrayscalePixel]()
            for (x <- 0 until dimX) {
                pixelLine.addOne(pixelArray(y)(dimX - 1 - x))
            }
            pixel2DArray.addOne(pixelLine.toArray)
        }
        pixelArray = pixel2DArray.toArray
    }

    private def rotate90(): Unit = {
        var pixel2DArray = ArrayBuffer[Array[GrayscalePixel]]()
        for (x <- 0 until dimX) {
            var pixelLine = ArrayBuffer[GrayscalePixel]()
            for (y <- 0 until dimY) {
                pixelLine.addOne(pixelArray(dimY - 1 - y)(x))
            }
            pixel2DArray.addOne(pixelLine.toArray)
        }
        val oldX = dimX
        val oldY = dimY
        dimX = oldY
        dimY = oldX
        pixelArray = pixel2DArray.toArray
    }

    private def scaleTwice(): Unit = {
        var pixel2DArray = ArrayBuffer[Array[GrayscalePixel]]()
        for (y <- 0 until dimY) {
            var pixelLine = ArrayBuffer[GrayscalePixel]()
            for (x <- 0 until dimX) {
                pixelLine.addOne(pixelArray(y)(x))
                pixelLine.addOne(pixelArray(y)(x))
            }
            pixel2DArray.addOne(pixelLine.toArray)
            pixel2DArray.addOne(pixelLine.toArray)
        }
        dimX *= 2
        dimY *= 2
        pixelArray = pixel2DArray.toArray
    }

    private def scaleHalf(): Unit = {
        var pixel2DArray = ArrayBuffer[Array[GrayscalePixel]]()
        for (y <- 0 until dimY/2) {
            var pixelLine = ArrayBuffer[GrayscalePixel]()
            for (x <- 0 until dimX/2) {
                var accum: Int = pixelArray(2*y    )(2*x    ).luma
                accum +=         pixelArray(2*y + 1)(2*x    ).luma
                accum +=         pixelArray(2*y    )(2*x + 1).luma
                accum +=         pixelArray(2*y + 1)(2*x + 1).luma
                accum /= 4
                var pixel = new GrayscalePixel(0)
                pixel.luma = accum
                pixelLine.addOne(pixel)
            }
            pixel2DArray.addOne(pixelLine.toArray)
        }
        dimX /= 2
        dimY /= 2
        pixelArray = pixel2DArray.toArray
    }
}
