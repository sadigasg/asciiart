package com.asciiart.image.transform

sealed trait ScalingFactor
case object Half extends ScalingFactor
case object Full extends ScalingFactor
case object Twice extends ScalingFactor
