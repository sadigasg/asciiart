package com.asciiart.image.transform

sealed trait RotationFactor
case object Deg_90 extends RotationFactor
case object Deg_180 extends RotationFactor
case object Deg_270 extends RotationFactor
