package com.asciiart.image.transform

trait Rotatable {

    def rotate(factor: RotationFactor): Unit
}
