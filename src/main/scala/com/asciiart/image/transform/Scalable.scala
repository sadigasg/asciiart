package com.asciiart.image.transform

trait Scalable {

    def scale(factor: ScalingFactor): Unit
}
