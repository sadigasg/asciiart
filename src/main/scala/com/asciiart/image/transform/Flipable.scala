package com.asciiart.image.transform

trait Flipable {

    def flip(flip: AxisFlip): Unit
}
