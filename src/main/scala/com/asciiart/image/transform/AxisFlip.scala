package com.asciiart.image.transform

sealed trait AxisFlip
case object XFlip extends AxisFlip
case object YFlip extends AxisFlip