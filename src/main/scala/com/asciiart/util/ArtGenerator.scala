package com.asciiart.util

import com.asciiart.image.Image

class ArtGenerator(var palette: String = (" .:-=+*#%@").reverse) {

    def convert(image: Image): String = {
        var artText: String = ""
        for (y <- 0 until image.dimY) {
            for (x <- 0 until image.dimX) {
                val discErr: Double = 255.0 / (palette.length - 1)
                val charId: Int = (image.getPixel(y)(x).luma / discErr).toInt
                artText += palette(charId)
            }
            artText += "\r\n"
        }
        artText
    }
}
