package com.asciiart.output

trait Writer {

    def write(art: String): Unit
}
