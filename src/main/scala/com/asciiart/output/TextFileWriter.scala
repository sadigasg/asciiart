package com.asciiart.output

import java.io.{BufferedWriter, File, FileWriter}

class TextFileWriter(var filePath: String) extends Writer {

    override
    def write(art: String): Unit = {
        val writer = new BufferedWriter(new FileWriter(filePath))
        writer.write(art)
        writer.close()
    }
}
