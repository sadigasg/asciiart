package com.asciiart.output

class ConsoleWriter extends Writer {

    override
    def write(art: String): Unit = {
        println(art)
    }
}
