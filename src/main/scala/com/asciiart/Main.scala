package com.asciiart

import com.asciiart.commands._
import com.asciiart.image.loader.FileLoader

import scala.collection.mutable.ArrayBuffer
import scala.sys.exit

object Main {

    def usage: String = {
        "Usage: asciiart --image <IMAGE PATH> [options]" + "\n" +
        "Help:  asciiart --help" + "\n" +
        "       asciiart -h" + "\n"
    }

    def launch(args: Array[String]): Unit = {

        var argsList = args.toList

        if (argsList.isEmpty) {
            print(usage)
            exit(1)
        }

        var imageLoader = new FileLoader("")

        def parse(commands : ArrayBuffer[Command], list: List[String]) : Unit = {

            if (list.isEmpty) {
                return
            }

            list match {
                case "--help" :: tail => {
                    val source = scala.io.Source.fromResource("user-manual.txt")
                    val lines: String = {
                        try source.getLines().mkString("\n") finally source.close()
                    }
                    print(lines)
                }
                case "-h" :: tail => {
                    val source = scala.io.Source.fromResource("user-manual.txt")
                    val lines: String = {
                        try source.getLines().mkString("\n") finally source.close()
                    }
                    print(lines)
                }
                case "--image" :: path :: tail => {
                    val format = path.takeRight(3)
                    format match {
                        case "jpg" | "png" | "gif" => imageLoader = new FileLoader(path)
                        case _ => {
                            println("Unsupported file format, supported file formats are jpg, png & gif")
                            println
                        }
                    }
                    parse(commands, tail)
                }
                case "--rotate" :: value :: tail =>
                    parse(commands.prepend(new RotateCommand(value.toInt)), tail)
                case "--scale" :: value :: tail =>
                    parse(commands.prepend(new ScaleCommand(value.toDouble)), tail)
                case "--invert" :: tail =>
                    parse(commands.prepend(new InvertCommand), tail)
                case "--brightness" :: value :: tail =>
                    parse(commands.prepend(new BrightnessCommand(value.toInt)), tail)
                case "--flip" :: axis :: tail =>
                    parse(commands.prepend(new FlipCommand(axis)), tail)
                case "--output-console" :: tail =>
                    parse(commands.append(new PrintToConsoleCommand), tail)
                case "--output-file" :: path :: tail =>
                    parse(commands.append(new SaveToFileCommand(path)), tail)
                case option :: tail => {
                    print(usage)
                    exit(1)
                }
            }
        }



        var commands = new ArrayBuffer[Command]()

        parse(commands, argsList)

        var image = imageLoader.load()
        if (image == null) {
            exit(1)
        }

        for (command <- commands) {
            command.execute(image)
        }
    }
}
