package com.asciiart.commands

import _root_.com.asciiart.image.transform.{Deg_180, Deg_270, Deg_90}
import com.asciiart.image.Image

class RotateCommand(value: Int) extends Command {
    override
    def execute(image: Image): Unit = {

        if (value % 90 != 0) {
            println("Invalid angle, Rotation wont apply")
        }

        var angle = value % 360
        if (angle < 0) {
          angle = 360 + angle
        }
        angle match {
            case 90 => image.rotate(Deg_90)
            case 180 => image.rotate(Deg_180)
            case 270 => image.rotate(Deg_270)
            case 0 => {}
            case _ => {
                println("Invalid angle, Rotation wont apply")
            }
        }
    }
}
