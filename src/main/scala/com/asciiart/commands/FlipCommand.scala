package com.asciiart.commands
import _root_.com.asciiart.image.transform.{XFlip, YFlip}
import com.asciiart.image.Image

class FlipCommand(axis: String) extends Command {

    override
    def execute(image: Image): Unit = {

        if (axis.length != 1) {
            println("Invalid axis, Flip wont apply")
        }

        axis match {
            case "x" => image.flip(XFlip)
            case "X" => image.flip(XFlip)
            case "y" => image.flip(YFlip)
            case "Y" => image.flip(YFlip)
            case _ => {
                println("Invalid axis, Flip wont apply")
            }
        }
    }
}
