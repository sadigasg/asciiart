package com.asciiart.commands
import com.asciiart.image.Image
import com.asciiart.image.pixel.filter.GrayscaleInverse

class InvertCommand extends Command {

    override
    def execute(image: Image): Unit = {

        image.applyFilter(new GrayscaleInverse)
    }
}
