package com.asciiart.commands

import com.asciiart.image.Image
import com.asciiart.image.pixel.filter.GrayscaleBrightness

class BrightnessCommand(value: Int) extends Command {

    override
    def execute(image: Image): Unit = {

        image.applyFilter(new GrayscaleBrightness(value))
    }
}
