package com.asciiart.commands

import com.asciiart.image.Image
import com.asciiart.output.TextFileWriter
import com.asciiart.util.ArtGenerator

class SaveToFileCommand(filepath: String) extends Command {

    override
    def execute(image: Image): Unit = {

        val generator = new ArtGenerator
        val writer = new TextFileWriter(filepath)
        writer.write(generator.convert(image))
    }
}
