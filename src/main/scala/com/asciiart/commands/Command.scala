package com.asciiart.commands

import com.asciiart.image.Image

trait Command {

  def execute(image: Image)
}
