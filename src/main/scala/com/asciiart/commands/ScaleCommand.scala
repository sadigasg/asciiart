package com.asciiart.commands

import _root_.com.asciiart.image.transform.{Half, Twice}
import com.asciiart.image.Image

class ScaleCommand(value: Double) extends Command {

    override
    def execute(image: Image): Unit = {
        value match {
            case 4 => image.scale(Twice)
            case 0.25 => image.scale(Half)
            case 1 => {}
            case _ => {
                println("Invalid scaling factor, Scaling wont apply")
            }
        }
    }
}
