package com.asciiart.commands

import com.asciiart.image.Image
import com.asciiart.output.ConsoleWriter
import com.asciiart.util.ArtGenerator

class PrintToConsoleCommand extends Command {

    override
    def execute(image: Image): Unit = {
        val generator = new ArtGenerator
        val writer = new ConsoleWriter

        writer.write(generator.convert(image))
    }
}
