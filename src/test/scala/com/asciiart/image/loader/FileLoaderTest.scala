package com.asciiart.image.loader

import org.scalatest.funsuite.AnyFunSuite

class FileLoaderTests extends AnyFunSuite {

    test("Load supported image"){

        val loader = new FileLoader("images/google.png")
        val image = loader.load()
        assert(image != null)
    }

    test("Load non-supported image") {

        val loader = new FileLoader("images/unsupported.txt")
        val image = loader.load()
        assert(image == null)
    }

    test("Load non-existing image") {

        val loader = new FileLoader("images/doesnt-exist.jpg")
        val image = loader.load()
        assert(image == null)
    }
}
