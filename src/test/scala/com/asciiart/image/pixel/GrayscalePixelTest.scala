package com.asciiart.image.pixel

import com.asciiart.image.pixel.format.GrayscalePixel
import org.scalatest.funsuite.AnyFunSuite

class GrayscalePixelTests extends AnyFunSuite {

    test("Create from RGB White") {

        var pixel = new GrayscalePixel(0xFFFFFFFF)
        assert(pixel.luma == 255)
    }

    test("Create from RGB Black") {

        var pixel = new GrayscalePixel(0)
        assert(pixel.luma == 0)
    }

    test("Create from RGB Arbitrary") {

        var pixel = new GrayscalePixel(0x89A40DCF)
        assert(pixel.luma == 79)
    }

    test("Set luma normal") {

        var pixel = new GrayscalePixel(0)
        pixel.luma = 140
        assert(pixel.luma == 140)
    }

    test("Set luma too high") {

        var pixel = new GrayscalePixel(0)
        pixel.luma = 1000000
        assert(pixel.luma == 255)
    }

    test("Set luma too low") {

        var pixel = new GrayscalePixel(0)
        pixel.luma = -1000000
        assert(pixel.luma == 0)
    }
}
