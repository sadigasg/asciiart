package com.asciiart.image.pixel

import com.asciiart.image.pixel.filter.GrayscaleBrightness
import com.asciiart.image.pixel.format.GrayscalePixel
import org.scalatest.funsuite.AnyFunSuite

class GrayscaleBrightnessTests extends AnyFunSuite {

    test("Apply neutral") {

        var pixel = new GrayscalePixel(0)
        var filter = new GrayscaleBrightness(0)
        filter.apply(pixel)
        assert(pixel.luma == 0)
    }

    test("Apply normal") {

        var pixel = new GrayscalePixel(0)
        var filter = new GrayscaleBrightness(100)
        filter.apply(pixel)
        assert(pixel.luma == 100)
    }

    test("Apply positive overflow") {

        var pixel = new GrayscalePixel(0)
        pixel.luma = 100
        var filter = new GrayscaleBrightness(2147483647)
        filter.apply(pixel)
        assert(pixel.luma == 255)
    }

    test("Apply negative overflow") {

        var pixel = new GrayscalePixel(0)
        pixel.luma = 100
        var filter = new GrayscaleBrightness(-2147483647)
        filter.apply(pixel)
        assert(pixel.luma == 0)
    }
}
