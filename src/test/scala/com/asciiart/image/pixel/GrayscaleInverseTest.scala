package com.asciiart.image.pixel

import com.asciiart.image.pixel.filter.GrayscaleInverse
import com.asciiart.image.pixel.format.GrayscalePixel
import org.scalatest.funsuite.AnyFunSuite

class GrayscaleInverseTest extends AnyFunSuite {

    test("Apply to black") {

        var pixel = new GrayscalePixel(0)
        var filter = new GrayscaleInverse()
        filter.apply(pixel)
        assert(pixel.luma == 255)
    }

    test("Apply to white") {

        var pixel = new GrayscalePixel(0xFFFFFFFF)
        var filter = new GrayscaleInverse()
        filter.apply(pixel)
        assert(pixel.luma == 0)
    }

    test("Apply to arbitrary") {

        var pixel = new GrayscalePixel(0x89A40DCF)
        var filter = new GrayscaleInverse()
        filter.apply(pixel)
        assert(pixel.luma == 176)
    }
}
