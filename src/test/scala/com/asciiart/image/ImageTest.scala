package com.asciiart.image

import com.asciiart.image.pixel.format.GrayscalePixel
import com.asciiart.image.transform.{Deg_90, Half, Twice, XFlip, YFlip}

import scala.collection.mutable.ArrayBuffer
import org.scalatest.funsuite.AnyFunSuite

class ImageTest extends AnyFunSuite {

    test("Create normal image") {
        var pixelLine = new ArrayBuffer[GrayscalePixel](0)
        var pix = new GrayscalePixel(0x00000000)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xF0F0F0F0)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0x0F0F0F0F)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xFFFFFFFF)
        pixelLine.addOne(pix)

        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        pixelArray.addOne(pixelLine.toArray)
        pixelArray.addOne(pixelLine.toArray)

        var image = new Image(4, 2, pixelArray.toArray)

        for (y <- 0 until image.dimY) {
            for (x <- 0 until image.dimX) {
                assert(image.getPixel(y)(x) == pixelLine(x))
            }
        }
    }

    test("Create with wrong pixel array (x axis)") {
        var pixelLine = new ArrayBuffer[GrayscalePixel](0)
        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        var pix = new GrayscalePixel(0x00000000)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xF0F0F0F0)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0x0F0F0F0F)
        pixelLine.addOne(pix)
        pixelArray.addOne(pixelLine.toArray)
        pix = new GrayscalePixel(0xFFFFFFFF)
        pixelLine.addOne(pix)
        pixelArray.addOne(pixelLine.toArray)

        var msg: String = ""
        try {
            var image = new Image(4, 2, pixelArray.toArray)
        } catch {
            case a : Throwable => msg = a.getMessage
        }
        assert(msg == "Pixel array has wrong X dimension.")
    }

    test("Create with wrong pixel array (y axis)") {
        var pixelLine = new ArrayBuffer[GrayscalePixel](0)
        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        var pix = new GrayscalePixel(0x00000000)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xF0F0F0F0)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0x0F0F0F0F)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xFFFFFFFF)
        pixelLine.addOne(pix)
        pixelArray.addOne(pixelLine.toArray)

        var msg: String = ""
        try {
            var image = new Image(4, 2, pixelArray.toArray)
        } catch {
            case a : Throwable => msg = a.getMessage
        }
        assert(msg == "Pixel array has wrong Y dimension.")
    }

    test("Create with X = 0") {
        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        var msg: String = ""
        try {
            var image = new Image(0, 2, pixelArray.toArray)
        } catch {
            case a : Throwable => msg = a.getMessage
        }
        assert(msg == "Unsupported resolution.")
    }

    test("Create with Y = 0") {
        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        var msg: String = ""
        try {
            var image = new Image(4, 0, pixelArray.toArray)
        } catch {
            case a : Throwable => msg = a.getMessage
        }
        assert(msg == "Unsupported resolution.")
    }

    test("Create with X and Y = 0") {
        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        var msg: String = ""
        try {
            var image = new Image(0, 0, pixelArray.toArray)
        } catch {
            case a : Throwable => msg = a.getMessage
        }
        assert(msg == "Unsupported resolution.")
    }

    test("Scale 0.25x") {
        var pixelLine = new ArrayBuffer[GrayscalePixel](0)
        var pix = new GrayscalePixel(0x00000000)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xF0F0F0F0)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0x0F0F0F0F)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xFFFFFFFF)
        pixelLine.addOne(pix)

        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        pixelArray.addOne(pixelLine.toArray)
        pixelArray.addOne(pixelLine.toArray)

        var image = new Image(4, 2, pixelArray.toArray)
        image.scale(Half)

        assert(image.getPixel(0)(0).luma == 120)
        assert(image.getPixel(0)(1).luma == 135)
    }

    test("Scale 4x") {
        var pixelLine = new ArrayBuffer[GrayscalePixel](0)
        var pix = new GrayscalePixel(0x00000000)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xF0F0F0F0)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0x0F0F0F0F)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xFFFFFFFF)
        pixelLine.addOne(pix)

        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        pixelArray.addOne(pixelLine.toArray)
        pixelArray.addOne(pixelLine.toArray)

        var image = new Image(4, 2, pixelArray.toArray)
        image.scale(Twice)

        for (y <- 0 until image.dimY) {
            for (x <- 0 until image.dimX) {
                assert(image.getPixel(y)(x) == pixelLine(x/2))
            }
        }
    }

    test("Rotate 90") {
        var pixelLine = new ArrayBuffer[GrayscalePixel](0)
        var pix = new GrayscalePixel(0x00000000)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xF0F0F0F0)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0x0F0F0F0F)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xFFFFFFFF)
        pixelLine.addOne(pix)

        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        pixelArray.addOne(pixelLine.toArray)
        pixelArray.addOne(pixelLine.toArray)

        var image = new Image(4, 2, pixelArray.toArray)
        image.rotate(Deg_90)

        for (y <- 0 until image.dimY) {
            for (x <- 0 until image.dimX) {
                assert(image.getPixel(y)(x) == pixelLine(y))
            }
        }
    }

    test("Flip X") {
        var pixelLine = new ArrayBuffer[GrayscalePixel](0)
        var pix = new GrayscalePixel(0x00000000)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xF0F0F0F0)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0x0F0F0F0F)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xFFFFFFFF)
        pixelLine.addOne(pix)

        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        pixelArray.addOne(pixelLine.toArray)
        pixelArray.addOne(pixelLine.toArray)

        var image = new Image(4, 2, pixelArray.toArray)
        image.flip(XFlip)

        for (y <- 0 until image.dimY) {
            for (x <- 0 until image.dimX) {
                assert(image.getPixel(y)(x) == pixelLine(x))
            }
        }
    }

    test("Flip Y") {
        var pixelLine = new ArrayBuffer[GrayscalePixel](0)
        var pix = new GrayscalePixel(0x00000000)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xF0F0F0F0)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0x0F0F0F0F)
        pixelLine.addOne(pix)
        pix = new GrayscalePixel(0xFFFFFFFF)
        pixelLine.addOne(pix)

        var pixelArray = new ArrayBuffer[Array[GrayscalePixel]](0)
        pixelArray.addOne(pixelLine.toArray)
        pixelArray.addOne(pixelLine.toArray)

        var image = new Image(4, 2, pixelArray.toArray)
        image.flip(YFlip)

        for (y <- 0 until image.dimY) {
            for (x <- 0 until image.dimX) {
                assert(image.getPixel(y)(x) == pixelLine(image.dimX - 1 - x))
            }
        }
    }
}
