package com.asciiart.output

import org.scalatest.funsuite.AnyFunSuite

import java.util.Calendar

class TextFileWriterTest extends AnyFunSuite {

    test("Saving text to a file") {

        val now = Calendar.getInstance().getTime()
        val filePath = "images/test_output.txt"
        val writer = new TextFileWriter(filePath)
        val text = "Test running at " + now.toString
        writer.write(text)

        val lines = scala.io.Source.fromFile(filePath).mkString
        assert(lines == text)
    }
}
