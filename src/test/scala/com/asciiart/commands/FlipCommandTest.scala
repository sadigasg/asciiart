package com.asciiart.commands

import com.asciiart.image.Image
import com.asciiart.image.loader.FileLoader
import org.scalatest.funsuite.AnyFunSuite

class FlipCommandTests extends AnyFunSuite {

    test("Flip to X axis") {

        val loader = new FileLoader("images/small.png")
        var image: Image = loader.load()
        val command = new FlipCommand("x")
        var pixelArray = image.pixelArray

        command.execute(image)

        assert(pixelArray != image.pixelArray)
    }

    test("Flip to Y axis") {

        val loader = new FileLoader("images/small.png")
        var image: Image = loader.load()
        val command = new FlipCommand("y")
        var pixelArray = image.pixelArray

        command.execute(image)

        assert(pixelArray != image.pixelArray)
    }

    test("Invalid Flip (other than x or y") {

        val loader = new FileLoader("images/small.png")
        val image: Image = loader.load()
        val command = new FlipCommand("a")
        val pixelArray = image.pixelArray

        command.execute(image)

        assert(pixelArray == image.pixelArray)
    }
}
