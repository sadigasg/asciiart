package com.asciiart.commands

import com.asciiart.image.Image
import com.asciiart.image.loader.FileLoader
import org.scalatest.funsuite.AnyFunSuite

class RotateCommandTest extends AnyFunSuite {

    test("Rotate to 90 degree") {

        val loader = new FileLoader("images/small.png")
        val image: Image = loader.load()
        val command = new RotateCommand(90)
        val pixelArray = image.pixelArray

        command.execute(image)

        assert(pixelArray != image.pixelArray)
    }

    test("Rotate to 180 degree") {

        val loader = new FileLoader("images/small.png")
        val image: Image = loader.load()
        val command = new RotateCommand(180)
        val pixelArray = image.pixelArray

        command.execute(image)

        assert(pixelArray != image.pixelArray)
    }

    test("Rotate to 270 degree") {

        val loader = new FileLoader("images/small.png")
        val image: Image = loader.load()
        val command = new RotateCommand(270)
        val pixelArray = image.pixelArray

        command.execute(image)

        assert(pixelArray != image.pixelArray)
    }

    test("Rotate to 360 degree") {

        val loader = new FileLoader("images/small.png")
        val image: Image = loader.load()
        val command = new RotateCommand(360)
        val pixelArray = image.pixelArray

        command.execute(image)

        assert(pixelArray == image.pixelArray)
    }
}
