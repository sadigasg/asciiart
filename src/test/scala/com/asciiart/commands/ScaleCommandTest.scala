package com.asciiart.commands

import com.asciiart.image.Image
import com.asciiart.image.loader.FileLoader
import org.scalatest.funsuite.AnyFunSuite

class ScaleCommandTest extends AnyFunSuite {

    test("Scale to 0.25") {

        val loader = new FileLoader("images/small.png")
        val image: Image = loader.load()
        val command = new ScaleCommand(0.25)
        val pixelArray = image.pixelArray

        command.execute(image)

        assert(pixelArray != image.pixelArray)
    }

    test("Scale to 4") {

        val loader = new FileLoader("images/small.png")
        val image: Image = loader.load()
        val command = new ScaleCommand(4)
        val pixelArray = image.pixelArray

        command.execute(image)

        assert(pixelArray != image.pixelArray)
    }

    test("Scale to other than 0.25 or 4") {

        val loader = new FileLoader("images/small.png")
        val image: Image = loader.load()
        val command = new ScaleCommand(2)
        val pixelArray = image.pixelArray

        command.execute(image)

        assert(pixelArray == image.pixelArray)
    }
}
