package com.asciiart.util

import com.asciiart.image.loader.FileLoader
import org.scalatest.funsuite.AnyFunSuite

class ArtGeneratorTest extends AnyFunSuite {

    test("Generate Art") {
        val loader = new FileLoader("images/test_gradient.png")
        val generator = new ArtGenerator
        assert(generator.convert(loader.load()) == generator.palette + "\r\n")
    }
}
