# ASCII Art Generator

![image info](./document/result.png)

## Introduction

In this project, we used **Scala** programming language to implement transforming an image into an ASCII Art and, optionally, applying filters.

Example commands (sbt-shell):

> run --image ../images/google.jpg --rotate 90 --scale 0.25 --invert --output-console

> run --image ../images/google.jpg --output-file ../images/google.txt

![image info](./document/run-sbt-shell.png)

### Index

* [Syntax](#syntax)

### Features

* [Flip Image](#flip-image)
* [Rotate Image](#rotate-image)
* [Scale Image](#scale-image)
* [Invert Image](#invert-image)
* [Console/File Output](#console/file-output)
* [Help](#help)


## Syntax

Usage:

    asciiart --image <IMAGE PATH> [options]

Here,

    [options]
    --help | -h                 Displays the help page guiding how to use the tool
    --image <IMAGE PATH>        Specifies Image Path for processing
    --output-console            Displays Ascii Art on the console
    --output-file <PATH>        Stores Ascii Art in a file
    --invert                    Specifies that image needs to invert
    --brightness <VALUE>        Specifies a factor to which image needs to bright
    --flip < x | y >            Specifies an axis to which image needs to flip
    --rotate < 90 | 180 | 270 > Specifies an angle to which image needs to rotate
    --scale <.25 | 4>           Specifies a factor to which image needs to scale



## Features


### Flip Image

Flips an image to X or Y axis.

Usage:

    asciiart --image <IMAGE PATH> --flip <X|Y>

Examples:

    asciiart --image D:/images/sample.jpg --output-console --flip x
    asciiart --image D:/images/google.jpg --output-console --flip y



### Rotate Image

Rotates an image to 90, 180, 270 degrees

Usage:

    asciiart --image <IMAGE PATH> --rotate <90|180|270>

Examples:

    asciiart --image D:/images/sample.jpg --output-console --rotate 180
    asciiart --image D:/images/google.jpg --output-console --rotate 90



### Scale Image

Scales an image to given factor (.25 or 4).

Usage:

    asciiart --image <IMAGE PATH> --scale <.25 | 4> 

Examples:

    asciiart --image D:/images/sample.jpg --output-console --scale .25
    asciiart --image D:/images/google.jpg --output-console --scale 4



### Invert Image

Inverts an image.

Usage:

    asciiart --image <IMAGE PATH> --invert

Examples:

    asciiart --image D:/images/sample.jpg --output-console --invert



### Console/File Output

Displays results on console or stores in a text file.

Usage:

    asciiart --image <IMAGE PATH> --output-console --output-file <PATH>

Examples:

    asciiart --image D:/images/sample.jpg --output-console --output-file D:/images/sample.txt
    asciiart --image D:/images/google.jpg --output-file D:/images/google.txt



## Help

Displays the help page guiding how to use the tool.

Usage:

    asciiart --help
    asciiart -h



## Repository

Source code is available at [GitHub page](https://gitlab.fit.cvut.cz/sadigasg/asciiart).


## Contact Info

Feel free to contact me to discuss any issues, questions, or comments.

My contact info can be found on my [GitHub page](https://gitlab.fit.cvut.cz/sadigasg).
